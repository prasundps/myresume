$(function(){

    Chart.pluginService.register({
		beforeDraw: function (chart) {
			if (chart.config.options.elements.center) {
        //Get ctx from string
        var ctx = chart.chart.ctx;
        
				//Get options from the center object in options
        var centerConfig = chart.config.options.elements.center;
      	var fontStyle = centerConfig.fontStyle || 'Arial';
				var txt = centerConfig.text;
        var color = centerConfig.color || '#000';
        var sidePadding = centerConfig.sidePadding || 20;
        var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
        //Start with a base font of 30px
        ctx.font = "2em " + fontStyle;
        
				//Get the width of the string and also the width of the element minus 10 to give it 5px side padding
        var stringWidth = ctx.measureText(txt).width;
        var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

        // Find out how much the font can grow in width.
        var widthRatio = elementWidth / stringWidth;
        var newFontSize = Math.floor(30 * widthRatio);
        var elementHeight = (chart.innerRadius * 2);

        // Pick a new font size so it will not be larger than the height of label.
        var fontSizeToUse = Math.min(newFontSize, elementHeight);

				//Set font settings to draw it correctly.
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
        var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
        ctx.font = fontSizeToUse+"px " + fontStyle;
        ctx.fillStyle = color;
        
        //Draw text in center
        ctx.fillText(txt, centerX, centerY);
			}
		}
	});


    //get the doughnut chart canvas
    var ctx1 = $("#doughnut-chartcanvas-1");
    var ctx2 = $("#doughnut-chartcanvas-2");
    var ctx3 = $("#doughnut-chartcanvas-3");
    var ctx4 = $("#doughnut-chartcanvas-4");  
    var ctx5 = $("#doughnut-chartcanvas-5");
    var ctx6 = $("#doughnut-chartcanvas-6");
    var ctx7 = $("#doughnut-chartcanvas-7");
    var ctx8 = $("#doughnut-chartcanvas-8");
    var ctx9 = $("#doughnut-chartcanvas-9");







    //doughnut chart data
    var data1 = {
        //labels: ["match1", "match2", "match3", "match4", "match5"],
        datasets: [
            {
                label: "English",
                data: [30,70],
                backgroundColor: [
                    "#DCDCDC",
                    "#9ACD32",
                ],
                borderColor: [
                    "#DCDCDC",
                    "#9ACD32",    
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data2 = {
        //labels: ["match1", "match2", "match3", "match4", "match5"],
        datasets: [
            {
                label: "Hindi",
                data: [20, 35],
                backgroundColor: [
                    "#DCDCDC",
                    "#9ACD32",
                ],
                borderColor: [
                    "#DCDCDC",
                    "#9ACD32",
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
    };
    
    //doughnut chart data
    var data3 = {
        //labels: ["match1", "match2", "match3", "match4", "match5"],
        datasets: [
            {
                label: "German",
                data: [20, 35],
                backgroundColor: [
                    "#DCDCDC",
                    "#9ACD32",
                ],
                borderColor: [
                    "#DCDCDC",
                    "#9ACD32",    
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
    };

    //doughnut chart data
    var data4 = {
        //labels: ["match1", "match2", "match3", "match4", "match5"],
        datasets: [
            {
                label: "Deep Learning",
                data: [20, 35],
                backgroundColor: [
                    "#DCDCDC",
                    "#027683",
                   
                ],
                borderColor: [
                    "#DCDCDC",
                    "#027683",
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
    };

    //doughnut chart data
    var data5 = {
        //labels: ["match1", "match2", "match3", "match4", "match5"],
        datasets: [
            {
                label: "Computer Vision",
                data: [20, 35],
                backgroundColor: [
                    "#DCDCDC",
                    "#027683",
                   
                ],
                borderColor: [
                    "#DCDCDC",
                    "#027683",
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
    };

    //doughnut chart data
    var data6 = {
        //labels: ["match1", "match2", "match3", "match4", "match5"],
        datasets: [
            {
                label: "NLP",
                data: [20, 35],
                backgroundColor: [
                    "#DCDCDC",
                    "#027683",,
                   
                ],
                borderColor: [
                    "#DCDCDC",
                    "#027683",
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
    };

    //doughnut chart data
    var data7 = {
        //labels: ["match1", "match2", "match3", "match4", "match5"],
        datasets: [
            {
                label: "C++",
                data: [20, 35],
                backgroundColor: [
                    "#DCDCDC",
                    "#027683",
                   
                ],
                borderColor: [
                    "#DCDCDC",
                    "#027683",
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
    };

    //doughnut chart data
    var data8 = {
        //labels: ["match1", "match2", "match3", "match4", "match5"],
        datasets: [
            {
                label: "Python",
                data: [20, 35],
                backgroundColor: [
                    "#DCDCDC",
                    "#027683",
                   
                ],
                borderColor: [
                    "#DCDCDC",
                    "#027683",
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
    };

    //doughnut chart data
    var data9 = {
        //labels: ["match1", "match2", "match3", "match4", "match5"],
        datasets: [
            {
                //label: "Matlab",
                data: [20, 35],
                backgroundColor: [
                    "#DCDCDC",
                    "#027683",
                   
                ],
                borderColor: [
                    "#DCDCDC",
                    "#027683",
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
    };


    //options
    var options1 = {
        responsive: true,
        elements: {
            center: {
                text: 'English',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20, // Defualt is 20 (as a percentage)
                innerRadius: "70%"
            }
        },
        
        cutoutPercentage: 80,
        tooltips: {enabled: false},
        hover: {mode: null},
    };

    //options
    var options2 = {
        responsive: true,
        elements: {
            center: {
                text: 'Hindi',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20, // Defualt is 20 (as a percentage)
                innerRadius: "70%"
            }
        },
        cutoutPercentage: 80,
        tooltips: {enabled: false},
        hover: {mode: null}
    };

    //options
    var options3 = {
        responsive: true,
        elements: {
            center: {
                text: 'German',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20, // Defualt is 20 (as a percentage)
                innerRadius: "70%"
            }
        },
        cutoutPercentage: 80,
        tooltips: {enabled: false},
        hover: {mode: null}
    };

    //options
    var options4 = {
        responsive: true,
        elements: {
            center: {
                text: 'Deep Learning',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20, // Defualt is 20 (as a percentage)
                innerRadius: "70%"
            }
        },
        cutoutPercentage: 80,
        tooltips: {enabled: false},
        hover: {mode: null}
    };

    //options
    var options5 = {
        responsive: true,
        elements: {
            center: {
                text: 'Computer Vision',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20, // Defualt is 20 (as a percentage)
                innerRadius: "70%"
            }
        },
        cutoutPercentage: 80,
        tooltips: {enabled: false},
        hover: {mode: null}
    };

    //options
    var options6 = {
        responsive: true,
        elements: {
            center: {
                text: 'NLP',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20, // Defualt is 20 (as a percentage)
                innerRadius: "70%"
            }
        },
        cutoutPercentage: 80,
        tooltips: {enabled: false},
        hover: {mode: null}
    };

    //options
    var options7 = {
        responsive: true,
        elements: {
            center: {
                text: 'C++',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20, // Defualt is 20 (as a percentage)
                innerRadius: "70%"
            }
        },
        cutoutPercentage: 80,
        tooltips: {enabled: false},
        hover: {mode: null}
    };

    //options
    var options8 = {
        responsive: true,
        elements: {
            center: {
                text: 'Python',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20, // Defualt is 20 (as a percentage)
                innerRadius: "70%"
            }
        },
        cutoutPercentage: 80,
        tooltips: {enabled: false},
        hover: {mode: null}
    };

    //options
    var options9 = {
        responsive: true,
        elements: {
            center: {
                text: 'Matlab',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20, // Defualt is 20 (as a percentage)
                innerRadius: "70%"
            }
        },
        cutoutPercentage: 80,
        tooltips: {enabled: false},
        hover: {mode: null}
    };
    //create Chart class object
    var chart1 = new Chart(ctx1, {
        type: "doughnut",
        data: data1,
        options: options1
    });

    //create Chart class object
    var chart2 = new Chart(ctx2, {
        type: "doughnut",
        data: data2,
        options: options2
    });

    //create Chart class object
    var chart3 = new Chart(ctx3, {
        type: "doughnut",
        data: data3,
        options: options3
    });

    //create Chart class object
    var chart4 = new Chart(ctx4, {
        type: "doughnut",
        data: data4,
        options: options4
    });

    //create Chart class object
    var chart5 = new Chart(ctx5, {
        type: "doughnut",
        data: data5,
        options: options5
    });
    //create Chart class object
    var chart6 = new Chart(ctx6, {
        type: "doughnut",
        data: data6,
        options: options6
    });
    //create Chart class object
    var chart7 = new Chart(ctx7, {
        type: "doughnut",
        data: data7,
        options: options7
    });
    //create Chart class object
    var chart8 = new Chart(ctx8, {
        type: "doughnut",
        data: data8,
        options: options8
    });
    //create Chart class object
    var chart9= new Chart(ctx9, {
        type: "doughnut",
        data: data9,
        options: options9
    });

});